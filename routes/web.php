<?php

use App\Http\Controllers\AuthController;
use App\Http\Controllers\CandidateController;
use App\Http\Controllers\EmployerController;
use App\Http\Controllers\VacancyController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [AuthController::class, 'index'])->name('login');
Route::get('/list-candidate/{id}',[CandidateController::class, 'dataCandidates'] );
Route::get('/list-employer',[EmployerController::class, 'dataEmployers'] );
Route::get('/list-vacancy',[VacancyController::class, 'dataVacancies'] );
Route::get('/login', [AuthController::class, 'index'])->name('login');
Route::get('/dashboard', [AuthController::class, 'indexDashboard']);
