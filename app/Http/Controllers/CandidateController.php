<?php

namespace App\Http\Controllers;

use GuzzleHttp\Client;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;
use Yajra\DataTables\Facades\DataTables;

class CandidateController extends Controller
{

    public function dataCandidates($id)
    {
        // $client = new Client();
        // $url = 'http://127.0.0.1:8000/api/v1/candidate/list-candidates';

        // $response = $client->request('GET', $url,[
        //     'verify' => false,
        // ]);

        //  $responseBody = json_decode($response->getBody());
        
        return view('candidate-page\list_candidate', compact('id'));
    }
}
