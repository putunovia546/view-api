<?php

namespace App\Http\Controllers;

use GuzzleHttp\Client;
use Illuminate\Http\Request;

class VacancyController extends Controller
{
    public function dataVacancies(Request $request)
    {
        return view('vacancy-page\list-vacancies');
    }
}
