<nav class="navbar navbar-expand-lg px-4 fixed-top shadow">
    <div class="container-fluid">
      <a class="navbar-brand fw-bold text-primary" href="#">Jobseeker.</a>
      <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarText" aria-controls="navbarText" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
      <div class="collapse navbar-collapse" id="navbarText">
        <ul class="navbar-nav me-auto mb-2 mb-lg-0">
          <li class="nav-item">
            <a class="nav-link" href="/list-employer">Employer</a>
          </li>
          <li class="nav-item" id="vacancy-url">
            <a class="nav-link active" aria-current="page" href="/list-vacancy">Vacancy</a>
          </li>
        </ul>
        <span class="navbar-text" id="logoutButton">
            <button type="button" value="" onclick="logout(this)" class="btn btn-outline-secondary  px-4 rounded">Logout</button>
        </span>
      </div>
    </div>
  </nav>
    
@push('js')
<script>
  function logout(event)
  {
      $.ajax({
          url: "http://127.0.0.1:8000/api/v1/logout",
          type: 'POST',
          dataType: "JSON",
          serverSide: true,
          processData: false,
          contentType: false,
          beforeSend: function ($request) {
                $request.setRequestHeader("Accept", "aplication/JSON");
                $request.setRequestHeader("Authorization", "Bearer "+localStorage.token);
            },
          success: function (data)
          {
              localStorage.setItem('token', data.token);
              window.location = '/login';
          },
          error: function (err)
          {
              console.log("error");
          }
      });
  };   

  var v_url = document.getElementById('vacancy-url')
  var logoutb = document.getElementById('logoutButton')
  if(localStorage.getItem('token') === null){
    v_url.style.visibility = 'hidden';
    logoutb.style.visibility = 'hidden';
  }
</script>
    
@endpush