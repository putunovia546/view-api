@extends('layouts.auth-app', ['status' => 'complete'])

@section('content')
  <div class="container border login-form bg-white rounded shadow-lg p-4">
      <form action="" method="POST"  id="loginForm" name="loginForm" class="bg-white">
        <h2 class="fw-bold text-center my-3">Login</h2>
        @csrf
        <div class="mb-3 px-4">
          <label for="email_user" class="form-label">Email address</label>
          <input type="email" class="form-control" id="email_user" name="email_user" aria-describedby="emailHelp">
        </div>
        <div class="mb-3 px-4">
          <label for="password" class="form-label">Password</label>
          <input type="password" class="form-control" id="password" name="password">
        </div>
        <div class="text-center my-4">
          <button type="submit" value="submit" class="btn btn-primary fw-bold loginUser px-4 rounded">LOGIN</button>
        </div>
      </form>
    </div>

    
@endsection

@push('js')
<script>
  $(document).on("submit", "form", function(event)
  {
      event.preventDefault();
      $.ajax({
          url: "http://127.0.0.1:8000/api/login",
          type: 'POST',
          dataType: "JSON",
          data: new FormData(this),
          serverSide: true,
          processData: false,
          contentType: false,
          success: function (data, status)
          {
              localStorage.setItem('token', data.token);
              window.location = '/list-vacancy';
              console.log('Login Berhasil');
          },
          error: function (err)
          {
              console.log("error");
          }
      });
  });   
</script>
    
@endpush