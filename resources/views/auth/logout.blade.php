@extends('layout', ['status' => 'complete'])

@section('content')
<div class="container  login-form bg-white border shadow-md px-4 py-5 rounded">
      <form action="" method="POST"  id="loginForm" name="loginForm" >
        <h3 class="fw-bold text-center my-4">Login</h3>
        @csrf
        <div class="mb-3">
          <label for="exampleInputEmail1" class="form-label">Email address</label>
          <input type="email" class="form-control" id="email_user" name="email_user" aria-describedby="emailHelp">
        </div>
        <div class="mb-3">
          <label for="exampleInputPassword1" class="form-label">Password</label>
          <input type="password" class="form-control" id="password" name="password">
        </div>
        <div class="text-center">
          <button type="submit" value="submit" class="btn btn-primary loginUser px-4 rounded">LOGIN</button>
        </div>
      </form>
</div>
    
@endsection


@push('js')
<script>
  $(document).on("click", function(event)
  {
      event.preventDefault();
      $.ajax({
          url: "http://127.0.0.1:8000/api/logout",
          type: 'POST',
          dataType: "JSON",
          data: new FormData(this),
          serverSide: true,
          processData: false,
          contentType: false,
          success: function (data, status)
          {
              localStorage.setItem('token', data.token);
              window.location = '/list-vacancy';
              console.log('Login Berhasil');
          },
          error: function (err)
          {
              console.log("error");
          }
      });
  });   
</script>
    
@endpush