@extends('layouts.layout', ['status' => 'complete'])

@section('content')
@include('layouts\navbar')

<div class="bg-white table-data">
    <h4 class="fw-bold text-center pb-3">List Employer</h4>
    <table class="table table-hover p-3" id="employerTable">
        <thead>
            <tr>
                <th>No</th>
                <th>Employer Name</th>
                <th>Email</th>
                <th>Phone</th>
                <th>Website</th>
                <th>Action</th>
            </tr>
        </thead>
    </table>
</div>

<div class="modal fade" id="detailModal" tabindex="-1" aria-labelledby="detailModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title" id="modalHeading"></h5>
            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
        </div>
        <div class="modal-body">
            <form action=""  id="df_employer" name="df_employer" method="GET">
                @csrf
                <div class="form-group ">
                    <label class="col-sm-12 fw-bold fw-bold">Employer Name</label>
                    <div class="col-sm-12">
                        <p id="employer_name" name="employer_name"></p>
                    </div>
                </div>
                <div class="form-group ">
                    <label class="col-sm-12 fw-bold ">Employer Address</label>
                    <div class="col-sm-12">
                        <p id="employer_address" name="employer_address"></p>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group ">
                            <label class="col-sm-12 fw-bold">Email</label>
                            <div class="col-sm-12">
                                <p  id="email" name="email"></p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group ">
                            <label class="col-sm-12 fw-bold">Mobile Phone</label>
                            <div class="col-sm-12">
                                <p id="mobile_phone" name="mobile_phone"></p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group ">
                            <label class="col-sm-12 fw-bold">Fax</label>
                            <div class="col-sm-12">
                                <p  id="fax" name="fax"></p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group ">
                            <label class="col-sm-12 fw-bold">Phone</label>
                            <div class="col-sm-12">
                                <p id="phone" name="phone"></p>
                            </div>
                        </div>
                    </div>
                </div>
                
                
                <div class="form-group ">
                    <label class="col-sm-12 fw-bold">Employee Name</label>
                    <div class="col-sm-12">
                        <p id="employee_name" name="employee_name"></p>
                    </div>
                </div>
                <div class="form-group ">
                    <label class="col-sm-12 fw-bold">Employee Email</label>
                    <div class="col-sm-12">
                        <p  id="employee_email" name="employee_email"></p>
                    </div>
                </div>
            </form>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
        </div>
    </div>
</div>
@endsection

@push('js')
<script>
    $(function() {
        
        var table = $('#employerTable').DataTable({
            processing: true,
            serverSide: true,
            searching: false,
            bLengthChange: false,
            ordering: false,
            bInfo: false,
            
            ajax: "http://127.0.0.1:8000/api/v1/employer/list-employers",
            
            columns:[
                {data: 'DT_RowIndex', name: 'DT_RowIndex'},
                {data:'employer_name', name:'employer_name'},
                {data:'email', name: 'email'},
                {data:'phone', name: 'phone'},
                {data: 'website', name: 'website'},
                {data: 'employer_id', name: 'employer_id',
                    render: function(data) {
                        return "<a href='javascript:void(0)' id='"+data+"' class='f_employer tag text-decoration-none text-light bg-info p-2 rounded' onclick='showDetail(this)' data-toggle='modal' data-target='#detailModal'><i class='fas fa-eye mx-2'></i>Show</a>"
                    }}
            ]
        })
    })

    function showDetail(ele) {

        var id = $('#employerTable').DataTable().row($(ele).closest('tr')).data().employer_id;
        let _url = "http://127.0.0.1:8000/api/v1/employer/detail-employer";
        $.ajax({
            url: _url,
            data:{'employer_id':id},
            type: "POST",
            success: function(res) {
                $('#detailModal').modal('show');
                $('#modalHeading').html('Detail Employer');
                $('#df_employer').attr('data-id', res.data.employer_id);
                $('#employer_id').html(res.data.employer_id);
                $('#photo').html(res.data.photo);
                $('#employer_name').html(res.data.employer_name);
                $('#employer_address').html(res.data.employer_address);
                $('#email').html(res.data.email);
                $('#mobile_phone').html(res.data.mobile_phone);
                $('#phone').html(res.data.phone);
                $('#fax').html(res.data.fax);
                $('#employee_name').html(res.data.employee_name);
                $('#employee_email').html(res.data.employee_email);

            }
        })
    }
</script>
@endpush