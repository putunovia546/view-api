@extends('layouts.layout', ['status' => 'complete'])

@section('content')
@include('layouts\navbar')

<div class="bg-white table-data">
    <h4 class="fw-bold text-center pb-3">List Vacancy</h4>
    <table class="table table-hover p-4 mb-3" id="vacancyTable">
        <thead>
            <tr>
                <th>No</th>
                <th>Vacancy Name</th>
                <th>Min. Age</th>
                <th>Max. Age</th>
                <th>Position Status</th>
                <th>Location</th>
                <th>Action</th>
            </tr>
        </thead>
    </table>
</div>


<div class="modal fade" id="detailModal" tabindex="-1" aria-labelledby="detailModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content px-3">
            <div class="modal-header">
                <h4 class="modal-title" id="modalHeading"></h4>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <form action="" data-id="" id="df_vacancy" name="df_vacancy" method="POST">
                    @csrf
                    <p type="hidden" class="form-control" id="vacancy_id" name="vacancy_id"value="" readonly></p>
                    <div class="row mb-2 border-bottom">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="col-sm-12 control-label fw-bold">Vacancy Name</label>
                                <div class="col-sm-12">
                                    <p type="text" class="form-control border-0" id="vacancy_name" name="vacancy_name"value="" readonly></p>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="col-sm-12 control-label fw-bold">Location<i class="fas fa-map-marker mx-2"></i></label>
                                <div class="col-sm-12">
                                    <p type="text" class="form-control border-0" id="location" name="location" value="" readonly></p>
                                </div>
                            </div>
                        </div>
                    </div>
                    

                    <div class="row mb-2 border-bottom">
                        <div class="col-md-6">
                            <div class="form-group ">
                                <label class="col-sm-12 control-label fw-bold">Min Age</label>
                                <div class="col-sm-12">
                                    <p type="number" class="form-control border-0" id="min_age" name="min_age"value="" readonly></p>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group ">
                                <label class="col-sm-12 control-label fw-bold">Max Age</label>
                                <div class="col-sm-12">
                                    <p type="number" class="form-control border-0" id="max_age" name="max_age"value="" readonly></p>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="form-group mb-2">
                        <label class="col-sm-12 control-label fw-bold">Position Status</label>
                        <div class="col-sm-12">
                            <p type="text" class="form-control border-0" id="position_status" name="position_status"value="" readonly></p>
                        </div>
                    </div>

                    <div class="row mb-2 border-bottom">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="col-sm-12 control-label fw-bold">Other</label>
                                <div class="col-sm-12">
                                    <span class="form-control border-0" id="other" name="other" rows="6" readonly></span>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="col-sm-12 control-label fw-bold">Responsibility</label>
                                <div class="col-sm-12">
                                    <span class="border-0" id="responsibility" name="responsibility" rows="6" readonly></span>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                    
                   
                    <div class="form-group mb-2 ">
                        <label class="col-sm-12 control-label fw-bold ">Requirement</label>
                        <div class="col-sm-12">
                            <span class="form-control border-0" id="requirement" name="requirement" rows="5" readonly></span>
                        </div>
                    </div>
                    <div class="form-group mb-2">
                        <label class="col-sm-12 control-label fw-bold">Benefit</label>
                        <div class="col-sm-12">
                            <span class="form-control border-0" id="benefit" name="benefit" rows="5" readonly></span>
                        </div>
                    </div>
                    
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

@include('auth')
@endsection

@push('js')
<script>
    $(function() {

        // console.log(localStorage.token)
        var table = $('#vacancyTable').DataTable({
            processing: true,
            serverSide: true,
            searching: false,
            bLengthChange: false,
            ordering: false,
            bInfo: false,
            
            ajax: {
                url: "http://127.0.0.1:8000/api/v1/vacancy/list-vacancies",
                type: 'GET',
                beforeSend: function ($request) {
                    $request.setRequestHeader("Accept", "aplication/JSON");
                    $request.setRequestHeader("Authorization", "Bearer "+localStorage.token);
                }
            },
            
            columns:[
            {data: 'DT_RowIndex', name: 'DT_RowIndex'},
            {data:'vacancy_name', name:'vacancy_name'},
            {data:'min_age', name: 'min_age'},
            {data:'max_age', name: 'max_age'},
            {data: 'position_status', name: 'position_status'},
            {data: 'location', name: 'location'},
            {data: 'vacancy_id', name: 'vacancy_id',
                render: function(data) {
                    $btn =  "<a href='javascript:void(0)' id='"+data+"' class='f_vacancy text-decoration-none text-dark p-2 bg-info mx-2 text-white rounded tag ' onclick='showDetail(this)' data-toggle='modal' data-target='#detailModal'><i class='fas fa-eye mx-2'></i>Show</a>";
                    $btn = $btn + '' + " <a href='/list-candidate/"+data+"' id='"+data+"' class='vacancy_can bg-secondary p-2 text-decoration-none text-white tag'> <i class='fas fa-list-alt mx-2'></i>List Candidate</a>";

                    return $btn;
                }}
        ], 
        })
    })
    

    function showDetail(ele) {

        var id = $('#vacancyTable').DataTable().row($(ele).closest('tr')).data().vacancy_id;
        let _url = "http://127.0.0.1:8000/api/v1/vacancy/detail-vacancy";
        $.ajax({
            url: _url,
            data:{'vacancy_id':id},
            type: "POST",
            beforeSend: function ($request) {
                    $request.setRequestHeader("Accept", "aplication/JSON");
                    $request.setRequestHeader("Authorization", "Bearer "+localStorage.token);
                },
            success: function(res) {
                $('#detailModal').modal('show');
                $('#modalHeading').html('Detail vacancy');
                $('#df_vacancy').attr('data-id', res.data.vacancy_id);
                $('#vacancy_id').html(res.data.vacancy_id);
                $('#vacancy_name').html(res.data.vacancy_name);
                $('#min_age').html(res.data.min_age);
                $('#max_age').html(res.data.max_age);
                $('#other').html(res.data.other);
                $('#responsibility').html(res.data.responsibility);
                $('#requirement').html(res.data.requirement);
                $('#benefit').html(res.data.benefit);
                $('#position_status').html(res.data.position_status);
                $('#location').html(res.data.location);

            }
        })
    }
</script>
@endpush