@extends('layouts.layout', ['status' => 'complete'])

@section('content')

@include('layouts.navbar')
<div class="bg-white table-data">
    <h4 class="fw-bold text-center pb-3">List Candidate</h4>
    <table class="table table-hover px-4" id="candidateTable">
        <thead>
            <tr>
                <th>No</th>
                <th>Full Name</th>
                <th>Email</th>
                <th>Phone</th>
                <th>Gender</th>
                <th>Institution Name</th>
                <th>Action</th>
            </tr>
        </thead>
    </table>
</div>

<div class="modal fade" id="detailModal" tabindex="-1" aria-labelledby="detailModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title" id="modalHeading"></h5>
            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
        </div>
        <div class="modal-body">
            <form action="" data-id="" id="df_candidate" name="df_candidate" method="POST">
                @csrf
                {{-- <p type="hidden" class="form-control" id="candidate_id" name="candidate_id"value="" ></p> --}}
                <div class="row mb-3">
                    <div class="col-md-6">
                        <div class="form-group ">
                            <label class="col-sm-12 control-label fw-bold">Full Name</label>
                            <div class="col-sm-12">
                                <p type="text" class="form-control border-0" id="full_name" name="full_name"value="" ></p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="col-sm-12 control-label fw-bold">Nick Name</label>
                            <div class="col-sm-12">
                                <p type="text" class="form-control border-0" id="nick_name" name="nick_name"value="" ></p>
                            </div>
                        </div>
                    </div>
                </div>
               
                <div class="row mb-3">
                    <div class="col-md-6">
                        <div class="form-group mb-3 ">
                            <label class="col-sm-12 control-label fw-bold">Email</label>
                            <div class="col-sm-12">
                                <p type="email" class="form-control border-0" id="email" name="email"value="" ></p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group mb-3 ">
                            <label class="col-sm-12 control-label fw-bold">Phone</label>
                            <div class="col-sm-12">
                                <p type="text" class="form-control border-0" id="phone" name="phone"value="" ></p>
                            </div>
                        </div>
                    </div>
                </div>
               
                
                <div class="form-group mb-3 border-bottom">
                    <label class="col-sm-12 control-label fw-bold">Date Of Birth</label>
                    <div class="col-sm-12">
                        <p type="text" class="form-control border-0" id="dob" name="dob"value="" ></p>
                    </div>
                </div>
                
                <div class="row mb-3">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="col-sm-12 control-label fw-bold">Experience</label>
                            <div class="col-sm-12">
                                <p type="text" class="form-control border-0" id="experience" name="experience"value="" ></p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group mb-3">
                            <label class="col-sm-12 control-label fw-bold">Preferred Location</label>
                            <div class="col-sm-12">
                                <p type="text" class="form-control border-0" id="preferred_location" name="preferred_location"value="" ></p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row mb-3">
                    <div class="col-md-6">
                        <div class="form-group mb-3">
                            <label class="col-sm-12 control-label fw-bold">Link CV</label>
                            <div class="col-sm-12">
                                <a href="" id="tag-cv">
                                    <p  type="text" class="form-control border-0" role="button" id="cv" name="cv"value="" ></p>
                                </a> 
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group mb-3">
                            <label class="col-sm-12 control-label fw-bold">Institution Name</label>
                            <div class="col-sm-12">
                                <p type="text" class="form-control border-0" id="institution_name" name="institution_name"value="" ></p>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
        </div>
    </div>
</div>
    

@include('auth')
@endsection

@push('js')
<script>
    const url_ = window.location.href;
    var vacancy_id = url_.substring(url_.lastIndexOf('/') + 1);
    // console.log(id);
    $(function () {
        var table = $('#candidateTable').DataTable({
            processing: true,
            serverSide: true,
            searchDelay: 1000, // debounce delay search
            // bFilter: true,
            searching: false,
            bLengthChange: false,
            ordering: false,
            bInfo: false,

            ajax: {
                url: "http://127.0.0.1:8000/api/v1/candidate/list-candidates",
                type: 'GET',
                data: {'vacancy_id': vacancy_id},
                beforeSend: function ($request) {
                    $request.setRequestHeader("Accept", "aplication/JSON");
                    $request.setRequestHeader("Authorization", "Bearer "+localStorage.token);
                }
            },
            columns: [
                {data: 'DT_RowIndex', name: 'DT_RowIndex'},
                {data: 'full_name', name: 'full_name'},
                // {data: 'nick_name', name: 'nick_name'},
                // {data: 'dob', name: 'dob'},
                {data: 'email', name: 'email'},
                {data: 'phone', name: 'phone'},
                {data: 'gender', name: 'gender'},
                {data: 'institution_name', name: 'institution_name'},
                {data: 'candidate_id', name: 'candidate_id', 
                    render: function (data) {
                        return "<a href='javascript:void(0)' id='"+data+"' class='f_candidate tag text-decoration-none text-light bg-info p-2 rounded' onclick='showDetail(this)' data-toggle='modal' data-target='#detailModal'><i class='fas fa-eye mx-2'></i>Show</a>"}}    
            ]
        });
    });

    function showDetail(ele) {

        var id = $('#candidateTable').DataTable().row($(ele).closest('tr')).data().candidate_id;
        let _url = "http://127.0.0.1:8000/api/v1/candidate/detail-candidate";
        $.ajax({
            url: _url,
            data:{'candidate_id':id},
            type: "POST",
            beforeSend: function ($request) {
                    $request.setRequestHeader("Accept", "aplication/JSON");
                    $request.setRequestHeader("Authorization", "Bearer "+localStorage.token);
                },
            success: function(res) {
                $('#detailModal').modal('show');
                $('#modalHeading').html('Detail Candidate');
                $('#df_candidate').attr('data-id', res.data.candidate_id);
                $('#candidate_id').html(res.data.candidate_id);
                $('#photo').html(res.data.photo);
                $('#full_name').html(res.data.full_name);
                $('#nick_name').html(res.data.nick_name);
                $('#dob').html(res.data.dob);
                $('#email').html(res.data.email);
                $('#phone').html(res.data.phone);
                $('#experience').html(res.data.experience + ' years');
                $('#preferred_location').html(res.data.preferred_location);
                $('#cv').html(res.data.cv);
                $('#institution_name').html(res.data.institution_name);
                $('#tag-cv').attr('href','https://assets.karirpad.com/uploads/candidate/resume/'+res.data.cv );
            }
        })
    }
</script>
@endpush