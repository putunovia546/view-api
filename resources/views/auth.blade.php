@push('js')
<script>
    $(function () {
        if(typeof localStorage.token == "undefined")
        {
            window.location.href = "{{ route('login') }}"
        }
    })
</script>
    
@endpush